﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Oficio
    {
        [PrimaryKey]
        [JsonProperty("idOficio")]
        public int Id { get; set; }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("tipo")]
        public int Tipo { get; set; }
    }
}
