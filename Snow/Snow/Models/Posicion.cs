﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Posicion
    {
        [PrimaryKey]
        [JsonProperty("idPosicion")]
        public int Id { get; set; }

        [JsonProperty("idUsuario")]
        public int IdUsuario { get; set; }

        [JsonProperty("latitud")]
        public double Latitud { get; set; }

        [JsonProperty("longitud")]
        public double Longitud { get; set; }

        [JsonProperty("estado")]
        public int Estado { get; set; }

        [JsonProperty("fechaActual")]
        public DateTime FechaActual { get; set; }
    }
}
