﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Usuario
    {
        [PrimaryKey]
        [JsonProperty("idUsuario")]
        public int Id { get; set; }

        [JsonProperty("nombre")]
        public string Nombre { get; set; }

        [JsonProperty("apellido")]
        public string Apellido { get; set; }

        [JsonProperty("rut")]
        public string Rut { get; set; }

        [JsonProperty("telefono")]
        public int Telefono { get; set; }

        [JsonProperty("mail")]
        public string Mail { get; set; }

        [JsonProperty("tipo")]
        public int Tipo { get; set; }

        [JsonProperty("estado")]
        public int Estado { get; set; }

        [JsonProperty("fecha")]
        public string Fecha { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

    }
}
