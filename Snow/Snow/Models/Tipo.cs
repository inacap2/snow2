﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Tipo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
