﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Calificacion
    {
        [PrimaryKey]
        [JsonProperty("idCalificacion")]
        public int Id { get; set; }

        [JsonProperty("idSolicitud")]
        public int IdSolicitud { get; set; }

        [JsonProperty("idUsuario")]
        public int IdUsuario { get; set; }

        [JsonProperty("puntuacion")]
        public int Puntuacion { get; set; }

        [JsonProperty("comentario")]
        public string Comentario { get; set; }
    }
}
