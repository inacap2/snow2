﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class AsociacionOficio
    {
        [PrimaryKey]
        [JsonProperty("idAsociacion")]
        public int IdAsociacion { get; set; }

        [JsonProperty("idSocio")]
        public int IdSocio { get; set; }

        [JsonProperty("idOficio")]
        public int IdOficio { get; set; }

        [JsonProperty("fechaInicioAsociacion")]
        public DateTime FechaInicioAsociacion { get; set; }

        [JsonProperty("estado")]
        public int estado { get; set; }

    }
}
