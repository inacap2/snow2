﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Solicitud
    {
        [PrimaryKey]
        [JsonProperty("idSolicitud")]
        public int Id { get; set; }

        [JsonProperty("idEmparejamiento")]
        public int IdEmparejamiento { get; set; }

        [JsonProperty("idCliente")]
        public int IdCliente { get; set; }

        [JsonProperty("idSocio")]
        public int IdSocio { get; set; }

        [JsonProperty("idOficio")]
        public int IdOficio { get; set; }

        [JsonProperty("fechaInicioSolicitud")]
        public DateTime FechaInicioSolicitud { get; set; }

        [JsonProperty("fechaFinSolicitud")]
        public DateTime FechaFinSolicitud { get; set; }

        [JsonProperty("estado")]
        public int Estado { get; set; }

        [JsonProperty("tipoSolicitud")]
        public int TipoSolicitud { get; set; }
    }
}
