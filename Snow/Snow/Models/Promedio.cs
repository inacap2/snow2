﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Promedio
    {
        [JsonProperty("promedio")]
        public string NotaPromediada { get; set; }
    }
}
