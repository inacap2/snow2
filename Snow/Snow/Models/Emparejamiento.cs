﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snow.Models
{
    public class Emparejamiento
    {
        [PrimaryKey]
        [JsonProperty("idEmparejamiento")]
        public int IdEmparejamiento { get; set; }
        [JsonProperty("nombre")]
        public string Nombre { get; set; }
    }
}
