﻿using Snow.ViewModels;
using Snow.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Snow
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // MainPage = new MainPage();
            MenuViewModel menuViewModel = new MenuViewModel();
            if (menuViewModel.Usuario != null)
            {
                if (menuViewModel.Usuario.Mail != null)
                {
                    MainPage = new NavigationPage(new Views.Menu());
                }
                else
                {
                    MainPage = new NavigationPage(new Login());
                }
            }
            else
            {
                MainPage = new NavigationPage(new Login());
            }
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
