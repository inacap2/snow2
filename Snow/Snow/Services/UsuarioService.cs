﻿using Newtonsoft.Json;
using Snow.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class UsuarioService
    {
        public ObservableCollection<Usuario> usuarios { get; set; }

        //public string _IPFIJA = "192.168.1.43";
        public const string _IPFIJA = "snowchile.net";


        public UsuarioService()
        {
            if (usuarios == null)
            {
                usuarios = new ObservableCollection<Usuario>();
            }
        }

        public ObservableCollection<Usuario> Consultar()
        {
            return usuarios;
        }

        public async Task<bool> Guardar(Usuario usuario)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/usuarios/nuevo";

            usuario.Estado = 1; //el estado inicial de todos los usuarios son 1: Vigente
            usuario.Fecha = DateTime.Today.ToString("yyyy-MM-dd");
            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(usuario);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PostAsync(url + urlInsert, contenidoRaw);


            //string algo = respuesta.StatusCode.ToString(); Eliminar esta parte
            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> Modificar(Usuario usuario)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/usuarios/modificar/" + usuario.Id;

            //usuario.Estado = 1; //el estado inicial de todos los usuarios son 1: Vigente
            //usuario.Fecha = DateTime.Today.ToString("dd/MM/yyyy");
            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(usuario);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PutAsync(url + urlInsert, contenidoRaw);


            
            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<Usuario> Leer(int id)
        {
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/usuarios/"+id;
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            List<Usuario> respuesta2 = JsonConvert.DeserializeObject<List<Usuario>>(contenido);

            string algo = respuesta2.ToString();
            for (int i = 0; i < respuesta2.Count; i++)
            {
                if (respuesta2[i].Id == id)
                {
                    return respuesta2[i];
                }
            }
            return null;
        }

        public async Task<Usuario> Leer(string correo, string password)
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/usuarios";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Usuario> respuesta2 = JsonConvert.DeserializeObject<List<Usuario>>(contenido);

                for (int i = 0; i < respuesta2.Count; i++)
                {
                    if (respuesta2[i].Mail == correo && respuesta2[i].Password == password)
                    {
                        return respuesta2[i];
                    }
                }
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new Usuario();
                throw;
            }
            
            return new Usuario();
        }
    }
}
