﻿using Snow.Models;
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class MenuService
    {

        public Usuario ObtenerUsuario()
        {
            try
            {
                var _platform = DependencyService.Get<ISQLiteConexion>();
                SQLiteConnection db = _platform.GetConnection();
                Usuario usuario = new Usuario();
                usuario = db.Table<Usuario>().First();
                return usuario;
            }
            catch (Exception)
            {
                return new Usuario();
            }
            
        }

        public void EliminarUsuario()
        {
            var _platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteConnection db = _platform.GetConnection();
            db.DropTable<Usuario>();
        }

        public bool ActualizarUsuario(Usuario usuario)
        {
            var _platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteConnection db = _platform.GetConnection();
            var respuesta= db.Update(usuario);
            
            if (respuesta>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
