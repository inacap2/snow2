﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Snow.Services;
using SQLite;
[assembly: Xamarin.Forms.Dependency(typeof(AndroidSQLiteConexion))]
namespace Snow.Services
{
    public class AndroidSQLiteConexion : ISQLiteConexion
    {

        private string GetPath()
        {
            var dbName = "datosActuales.db3";
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            return path;
        }


        public SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(GetPath());
        }
        public SQLiteAsyncConnection GetAsyncConnection()
        {
            return new SQLiteAsyncConnection(GetPath());
        }

        
    }
}
