﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Snow.Services
{
    public interface ISQLiteConexion
    {
        SQLiteConnection GetConnection();
        SQLiteAsyncConnection GetAsyncConnection();
    }
}
