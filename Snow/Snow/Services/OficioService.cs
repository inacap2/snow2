﻿using Newtonsoft.Json;
using Snow.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class OficioService
    {

        public ObservableCollection<AsociacionOficio> Asociaciones { get; set; }

        //public const string _IPFIJA = "192.168.1.43";
        public const string _IPFIJA = "snowchile.net";

        public OficioService()
        {
            if (Asociaciones == null)
            {
                Asociaciones = new ObservableCollection<AsociacionOficio>();
            }
        }

        public ObservableCollection<AsociacionOficio> Consultar()
        {
            return Asociaciones;
        }

        public async Task<bool> GuardarAsociacion(AsociacionOficio asociacion)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/asociaciones/nuevo";

            asociacion.estado = 1; //El estado es 1 por ahora
            asociacion.FechaInicioAsociacion = DateTime.Today;

            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(asociacion);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PostAsync(url + urlInsert, contenidoRaw);


            //string algo = respuesta.StatusCode.ToString(); Eliminar esta parte
            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<AsociacionOficio>> LeerAsociaciones()
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/asociaciones";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<AsociacionOficio> respuesta2 = JsonConvert.DeserializeObject<List<AsociacionOficio>>(contenido);
                return respuesta2;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new List<AsociacionOficio>();

            }
        }

        public async Task<AsociacionOficio> LeerAsociacion(int idUsuario)
        {
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/asociaciones";
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            List<AsociacionOficio> respuesta2 = JsonConvert.DeserializeObject<List<AsociacionOficio>>(contenido);

            for (int i = 0; i < respuesta2.Count; i++)
            {
                if (respuesta2[i].IdSocio == idUsuario)
                {
                    return respuesta2[i];
                }
            }
            return null;
        }


        public async Task<List<Oficio>> LeerOficios()
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/oficios";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Oficio> respuesta2 = JsonConvert.DeserializeObject<List<Oficio>>(contenido);
                return respuesta2;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new List<Oficio>();
                
            }

        }


        public async Task<Oficio> LeerOficio(int id)
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/oficios";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Oficio> respuesta2 = JsonConvert.DeserializeObject<List<Oficio>>(contenido);

                for (int i = 0; i < respuesta2.Count; i++)
                {
                    if (respuesta2[i].Id == id)
                    {
                        return respuesta2[i];
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new Oficio();

            }

        }


    }

    
}
