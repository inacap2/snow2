﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Snow.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class CalificacionService
    {
        //public const string _IPFIJA = "192.168.1.43";
        public const string _IPFIJA = "snowchile.net";

        public async Task<List<Calificacion>> LeerCalificaciones()
        {
            try
            {
                //Leer todas las calificaciones de la BD

                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/calificaciones";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Calificacion> respuesta2 = JsonConvert.DeserializeObject<List<Calificacion>>(contenido);
                return respuesta2;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new List<Calificacion>();

            }
        }

        public async Task<string> LeerCalificacionExacta(int idUsuario)
        {
            //Leer la calificacion EXACTA del usuario
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/calificacion/" +idUsuario;
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            var respuesta2 = JsonConvert.DeserializeObject<List<Promedio>>(contenido);

            for (int i = 0; i < respuesta2.Count; i++)
            {
                if (respuesta2[i].NotaPromediada != null)
                {
                    return respuesta2[i].NotaPromediada;
                }           
            }

            return "0";
   
            
        }


        public async Task<bool> LeerCalificacionSolicitud(int idSolicitud)
        {
            
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/calificacionsolicitud/" + idSolicitud;
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            try
            {
                var respuesta2 = JsonConvert.DeserializeObject<List<Calificacion>>(contenido);

                for (int i = 0; i < respuesta2.Count; i++)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<Calificacion> LeerCalificacionSolicitud(int idSolicitud, int tipo)
        {

            //Leer la calificacion seleccionada
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/calificacionsolicitud/" + idSolicitud;
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            try
            {
                var respuesta2 = JsonConvert.DeserializeObject<List<Calificacion>>(contenido);

                for (int i = 0; i < respuesta2.Count; i++)
                {
                    return respuesta2[i];
                }
                return new Calificacion();
            }
            catch (Exception)
            {
                return null;
            }

        }


        public async Task<bool> GuardarCalificacion(Calificacion calificacion)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/calificaciones/nuevo";


            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(calificacion);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PostAsync(url + urlInsert, contenidoRaw);

            var contents = await respuesta.Content.ReadAsStringAsync();

            if (contents.IndexOf("Nueva calificacion guardada") > 0 && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}
