﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Snow.Services
{
    public class Validador
    {
        //Las validaciones son verdaderas cuando existen errores en las entradas


        public bool EsBlancoVacio(string entrada)
        {

            
            if (String.IsNullOrEmpty(entrada) || String.IsNullOrWhiteSpace(entrada))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool MailCorrecto(string entrada)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(entrada, expresion))
            {
                if (Regex.Replace(entrada, expresion, String.Empty).Length != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}
