﻿using Newtonsoft.Json;
using Snow.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class PosicionSocioService
    {
        //public const string _IPFIJA = "192.168.1.43";
        public const string _IPFIJA = "snowchile.net";

        public async Task<List<Posicion>> LeerPosiciones()
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/posiciones";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Posicion> respuesta2 = JsonConvert.DeserializeObject<List<Posicion>>(contenido);
                return respuesta2;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new List<Posicion>();

            }
        }

        public async Task<Posicion> LeerPosicion(int idUsuario)
        {
            HttpClient cliente = new HttpClient();
            string link = "http://" + _IPFIJA + "/snow/public/api/posicion/" + idUsuario;
            var respuesta = cliente.GetAsync(link).Result;

            var contenido = await respuesta.Content.ReadAsStringAsync();
            List<Posicion> respuesta2 = new List<Posicion>();
            try
            {
                respuesta2 = JsonConvert.DeserializeObject<List<Posicion>>(contenido);
            }
            catch(Exception)
            {
                
            };
            

            
            for (int i = 0; i < respuesta2.Count; i++)
            {
                if (respuesta2[i].IdUsuario == idUsuario)
                {
                    return respuesta2[i];
                }
            }
            return new Posicion() { Id = 0};
        }


        public async Task<bool> GuardarPosicion(Posicion posicion)
        {

            await ModificarPosicionesAnteriores(posicion.IdUsuario);

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/posiciones/nuevo";

            posicion.Estado = 1; //el estado inicial de todos los usuarios son 1: Vigente
            posicion.FechaActual = DateTime.Today;
            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(posicion);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PostAsync(url + urlInsert, contenidoRaw);
            //var contenido = respuesta.Content.ReadAsStringAsync();
            //string contenido2 = contenido.Result;


            //string algo = respuesta.StatusCode.ToString(); Eliminar esta parte
            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<bool> ModificarPosicionesAnteriores(int idSocio)
        {
            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/posiciones/modificar/"+ idSocio;

            HttpClient cliente = new HttpClient();
            var respuesta = await cliente.PutAsync(url + urlInsert, null);

            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
