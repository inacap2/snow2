﻿using Newtonsoft.Json;
using Snow.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Snow.Services
{
    public class SolicitudService
    {
        //public string _IPFIJA = "192.168.1.43";
        public const string _IPFIJA = "snowchile.net";

        public async Task<bool> GuardarEmparejamiento(Emparejamiento emparejamiento)
        {
            //Aquí se crea un emparejamiento
            string urlEmpareja = "http://" + _IPFIJA + "/snow/public/api/emparejamientos/nuevo";

            HttpClient cliente1 = new HttpClient();
            var contenidoJson1 = JsonConvert.SerializeObject(emparejamiento);
            var buffer1 = Encoding.UTF8.GetBytes(contenidoJson1);
            var contenidoRaw1 = new ByteArrayContent(buffer1);
            contenidoRaw1.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta1 = await cliente1.PostAsync(urlEmpareja, contenidoRaw1);


            if (respuesta1 != null && respuesta1.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<int> LeerUltimoEmparejamiento()
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/emparejamientos";
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<Emparejamiento> respuesta2 = JsonConvert.DeserializeObject<List<Emparejamiento>>(contenido);

                int ultimoRegistro = respuesta2.Count-1;
                int ultimoId = 0;

                ultimoId = respuesta2[ultimoRegistro].IdEmparejamiento;


                return ultimoId;
            }
            catch (Exception)
            {
                //string algo = ex.Message;
                return 0;

            }
        }


        public async Task<bool> GuardarSolicitud(Solicitud solicitud)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/solicitudes/nuevo";

            solicitud.Estado = 1; //el estado inicial de todos los usuarios son 1: Vigente
            solicitud.FechaInicioSolicitud = DateTime.Today;
            //Agrego a través del webservice


            HttpClient cliente = new HttpClient();
            var contenidoJson = JsonConvert.SerializeObject(solicitud);
            var buffer = Encoding.UTF8.GetBytes(contenidoJson);
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PostAsync(url + urlInsert, contenidoRaw);


            
            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<bool> FinalizarSolicitud(int idSolicitud)
        {

            string url = "http://" + _IPFIJA + "/snow/public/api";
            string urlInsert = "/solicitudes/finsolicitud/" + idSolicitud;

            HttpClient cliente = new HttpClient();
            var buffer = Encoding.UTF8.GetBytes("Contenido");
            var contenidoRaw = new ByteArrayContent(buffer);
            contenidoRaw.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var respuesta = await cliente.PutAsync(url + urlInsert, contenidoRaw);

            if (respuesta != null && respuesta.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //Para leer las solicitudes
        public async Task<List<CustomSolicitud>> LeerSolicitudes(int idSocio)
        {
            try
            {
                HttpClient cliente = new HttpClient();
                string link = "http://" + _IPFIJA + "/snow/public/api/solicitudes/"+ idSocio;
                var respuesta = cliente.GetAsync(link).Result;

                var contenido = await respuesta.Content.ReadAsStringAsync();
                List<CustomSolicitud> respuesta2 = JsonConvert.DeserializeObject<List<CustomSolicitud>>(contenido);
                return respuesta2;
            }
            catch (Exception ex)
            {
                string algo = ex.Message;
                return new List<CustomSolicitud>();

            }

        }

    }
}
