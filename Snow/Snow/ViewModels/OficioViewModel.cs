﻿using Snow.Models;
using Snow.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Snow.ViewModels
{
    public class OficioViewModel : INotifyPropertyChanged
    {
        //public ObservableCollection<Oficio> Oficios { get; set; }

        OficioService oficioService = new OficioService();
        MenuService menuService = new MenuService();
        Usuario usuario = new Usuario();

        private List<Oficio> listOficiosBd = new List<Oficio>();
        public List<Oficio> ListOficiosBd
        {
            get { return listOficiosBd; }
            set { listOficiosBd = value; OnPropertyChanged(); }
        }


        public Command LeerOficiosCommand
        {
            get
            {
                return new Command(async () =>
            {
                IsBusy = true;
                ListOficiosBd = await oficioService.LeerOficios();
                IsBusy = false;
            }, () => !IsBusy);
            }
        }

        public Command BuscarOficioSelCommand
        {
            get
            {
                return new Command(async () =>
                {

                    IsBusy = true;
                    //Guardo en una base de datos SQLite la informacion del oficio seleccionado, para no tener que volver a consultar
                    var platform = DependencyService.Get<ISQLiteConexion>();
                    SQLiteAsyncConnection db = platform.GetAsyncConnection();

                    await db.DropTableAsync<Oficio>();
                    await db.CreateTableAsync<Oficio>();


                    //Aqui debe insertar el oficio en la BD local
                    await db.InsertAsync(SelectedOficio);

                    await App.Current.MainPage.Navigation.PushAsync(new Views.MapaPedido());
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command RegistrarAsocOficioCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    AsociacionOficio = new AsociacionOficio();
                    usuario = menuService.ObtenerUsuario();
                    //Asigno los datos obtenidos al objeto AsociacionOficio
                    AsociacionOficio.IdSocio = menuService.ObtenerUsuario().Id;                  
                    AsociacionOficio.IdOficio = SelectedOficio.Id;

                    bool exito = await oficioService.GuardarAsociacion(AsociacionOficio);

                    if (exito)
                    {
                        await App.Current.MainPage.DisplayAlert("Felicidades", "Usted ha registrado un oficio en su cuenta", "OK");
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Ha ocurrido un error al registrar el oficio", "OK");
                    }
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }

        private Oficio oficio = new Oficio();

        public Oficio Oficio
        {
            get { return oficio; }
            set
            {
                oficio = value;
                OnPropertyChanged();
            }
        }

        private AsociacionOficio asociacionOficio;

        public AsociacionOficio AsociacionOficio
        {
            get { return asociacionOficio; }
            set { asociacionOficio = value; OnPropertyChanged(); }
        }



        public OficioViewModel()
        {
            IsBusy = true;
            LeerOficiosCommand.Execute(null);
            IsBusy = false;
        }

        private Oficio selectedOficio;

        public Oficio SelectedOficio
        {
            get { return selectedOficio; }
            set { selectedOficio = value; OnPropertyChanged(); }
        }


        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
