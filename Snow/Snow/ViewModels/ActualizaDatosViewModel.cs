﻿using Snow.Models;
using Snow.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Snow.ViewModels
{
    public class ActualizaDatosViewModel : INotifyPropertyChanged
    {
        UsuarioService usuarioService = new UsuarioService();

        MenuService menuService = new MenuService();
        public static string _correoAnterior;


        private Usuario usuario = new Usuario();

        public Usuario Usuario
        {
            get { return usuario; }
            set
            {
                usuario = value;
                OnPropertyChanged();
            }
        }


        public ActualizaDatosViewModel()
        {
            usuario = menuService.ObtenerUsuario();
            _correoAnterior = usuario.Mail;

        }


    public Command BtnActualizaDatosCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    bool respuesta1 = menuService.ActualizarUsuario(usuario);
                    bool respuesta2 = await usuarioService.Modificar(usuario);

                    if (respuesta1 && respuesta2)
                    {
                        await App.Current.MainPage.DisplayAlert("Actualizado", "Usted ha actualizado sus datos con éxito", "OK");

                        if (!_correoAnterior.Equals(usuario.Mail))
                        {
                            await App.Current.MainPage.Navigation.PushAsync(new Views.Login());
                        }
                        else
                        {
                            await App.Current.MainPage.Navigation.PushAsync(new Views.Menu());
                        }
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Usted no ha actualizado sus datos", "OK");
                    }
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }


        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
