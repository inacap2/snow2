﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Snow.Models;
using Snow.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Snow.ViewModels
{
    public class MenuViewModel : INotifyPropertyChanged
    {


        MenuService menuService = new MenuService();
        PosicionSocioService posicionSocioService = new PosicionSocioService();

        private Usuario usuario = new Usuario();
        public Usuario Usuario
        {
            
            get { return usuario; }
            set
            {
                usuario = value;
                OnPropertyChanged();
            }
        }

        public MenuViewModel()
        {
            Usuario = menuService.ObtenerUsuario();
            CrearSwitchCell();
        }

        private TableView tableView;

        public TableView TableView
        {
            get { return tableView; }
            set { tableView = value; OnPropertyChanged(); }
        }


        public async void CrearSwitchCell()
        {
            var switchCell = new SwitchCell
            {
                Text = "Mostrarse en línea"
            };

            Posicion posicionActual = await posicionSocioService.LeerPosicion(Usuario.Id);
            if (posicionActual.Id == 0)
            {
                switchCell.On = false;
            }
            else
            {
                switchCell.On = true;
            }

            var tableSection = new TableSection
            {
                switchCell
            };

            TableView = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot
                {
                    tableSection
                }
            };

            if (Usuario.Tipo == 1)
            {
                TableView.IsEnabled = true;
                TableView.IsVisible = true;
            }
            else
            {
                TableView.IsVisible = false;
            }

            switchCell.OnChanged += async (sender, e) =>
            {
                IsBusy = true;
                if (e.Value == true)
                {
                    bool eleccion = await App.Current.MainPage.DisplayAlert("Activar", "¿Está seguro de mostrarse activo?", "Si", "No");

                    if (eleccion)
                    {
                        PosicionSocio = new Posicion();
                        var locator = CrossGeolocator.Current;
                        locator.DesiredAccuracy = 50;
                        var posicion = await locator.GetPositionAsync(TimeSpan.FromSeconds(2), null, true);
                        Position _position = new Position(posicion.Latitude, posicion.Longitude);


                        PosicionSocio.IdUsuario = Usuario.Id;
                        PosicionSocio.Latitud = _position.Latitude;
                        PosicionSocio.Longitud = _position.Longitude;

                        bool resultado = await posicionSocioService.GuardarPosicion(PosicionSocio);
                    }
                    else
                    {
                        switchCell.On = false;
                    }

                }
                else
                {
                    await posicionSocioService.ModificarPosicionesAnteriores(Usuario.Id);
                }
                

                IsBusy = false;
            };


        }



        private string mensajeInicial;

        public string MensajeInicial
        {
            get { return "Bienvenido " + usuario.Nombre + " " + usuario.Apellido; }
            set { mensajeInicial = value;
                OnPropertyChanged();
            }
        }

        private Posicion posicionSocio;

        public Posicion PosicionSocio
        {
            get { return posicionSocio; }
            set { posicionSocio = value; OnPropertyChanged(); }
        }


        public Command CerrarSesionCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    menuService.EliminarUsuario();
                    await App.Current.MainPage.DisplayAlert("Cerrar sesión", "Usted ha cerrado sesión", "OK");

                    await App.Current.MainPage.Navigation.PushAsync(new Views.Login());
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }

        public Command RealizaPedidoCommand {
            get {
                return new Command(async () =>
                {
                    IsBusy = true;
                    await App.Current.MainPage.Navigation.PushAsync(new Views.ListaPedido());
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }


        public Command ActualizaDatosCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    await App.Current.MainPage.Navigation.PushAsync(new Views.ActualizaDatos());
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }

        public Command RegistraOficioCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    await App.Current.MainPage.Navigation.PushAsync(new Views.RegistrarOficio());
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }


        public Command EstadoRequerCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    await App.Current.MainPage.Navigation.PushAsync(new Views.EstadoSolicitudes());
                    IsBusy = false;
                }, () => !IsBusy);
            }
        }




        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
