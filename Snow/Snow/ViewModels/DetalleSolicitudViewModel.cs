﻿using Snow.Models;
using Snow.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Snow.ViewModels
{
    public class DetalleSolicitudViewModel : INotifyPropertyChanged
    {

        UsuarioService usuarioService = new UsuarioService();
        OficioService oficioService = new OficioService();
        MenuService menuService = new MenuService();
        SolicitudService solicitudService = new SolicitudService();
        CalificacionService calificacionService = new CalificacionService();



        //Falta que el nombre de la solicitud se traiga para aca
        //Todo lo demás.

        public DetalleSolicitudViewModel()
        {
            Calificacion = new Calificacion();
            ObtenerSolicitudSeleccionadaGuardada();
            Usuario = menuService.ObtenerUsuario();
            
            ObtenerOficio();
            ObtenerContraparte();

            Tipos = new List<Tipo>();
            for (int i = 0; i < 6; i++)
            {
                Tipo tipo = new Tipo();
                if (i==0)
                {
                    tipo.Id = 0;
                    tipo.Nombre = "Seleccione una calificación.";
                }
                else
                {
                    tipo.Id = i;
                    tipo.Nombre = (i).ToString();
                }
                
                Tipos.Add(tipo);
            }

            

        }


        public async void ObtenerSolicitudSeleccionadaGuardada()
        {
            var platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteAsyncConnection db = platform.GetAsyncConnection();
            try
            {
                var algo = db.Table<CustomSolicitud>();
                SolicitudSeleccionada = await db.Table<CustomSolicitud>().FirstOrDefaultAsync();
                EstadoSolicitud = SolicitudSeleccionada.Estado;
            }
            catch (Exception)
            {

            }

        }

        private List<Tipo> tipos;   

        public List<Tipo> Tipos
        {
            get { return tipos; }
            set { tipos = value; OnPropertyChanged(); }
        }

        private int selectedIndexTipo;

        public int SelectedIndexTipo
        {
            get { return selectedIndexTipo; }
            set { selectedIndexTipo = value; OnPropertyChanged(); }
        }


        private int estadoSolicitud;

        public int EstadoSolicitud
        {
            get { return estadoSolicitud; }
            set { estadoSolicitud = value; OnPropertyChanged(); }
        }

        private bool existeCalificacionSolicitud;

        public bool ExisteCalificacionSolicitud
        {
            get { return existeCalificacionSolicitud; }
            set { existeCalificacionSolicitud = value; OnPropertyChanged(); }
        }

        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; OnPropertyChanged(); }
        }

        private Usuario usuarioContraparte;

        public Usuario UsuarioContraparte
        {
            get { return usuarioContraparte; }
            set { usuarioContraparte = value; OnPropertyChanged(); }
        }

        private string nombreCompletoContraparte;

        public string NombreCompletoContraparte
        {
            get { return nombreCompletoContraparte; }
            set { nombreCompletoContraparte = value; OnPropertyChanged(); }
        }

        private string comentario;

        public string Comentario
        {
            get { return comentario; }
            set { comentario = value; OnPropertyChanged(); }
        }


        public async void ObtenerContraparte()
        {
            try
            {
                var platform = DependencyService.Get<ISQLiteConexion>();
                SQLiteAsyncConnection db = platform.GetAsyncConnection();
                try
                {
                    var algo = db.Table<CustomSolicitud>();
                    SolicitudSeleccionada = await db.Table<CustomSolicitud>().FirstOrDefaultAsync();
                }
                catch (Exception)
                {

                }

                if (Usuario.Tipo == 1) //si es socio
                {
                    UsuarioContraparte = await usuarioService.Leer(SolicitudSeleccionada.IdCliente);
                }
                else if (Usuario.Tipo == 2) //Si es cliente
                {
                    UsuarioContraparte = await usuarioService.Leer(SolicitudSeleccionada.IdSocio);
                }
                //NombreCompletoContraparte = null;
                NombreCompletoContraparte = UsuarioContraparte.Nombre + " " + UsuarioContraparte.Apellido;
                ObtenerOficio();
                ObtenerCalificacionSolicitud();
                

                if (Usuario.Tipo == 1)
                {
                    Calificacion = await calificacionService.LeerCalificacionSolicitud(SolicitudSeleccionada.Id, 1);
                }
                else
                {
                    Calificacion.IdSolicitud = SolicitudSeleccionada.Id;
                }

            }
            catch (Exception)
            {

            }

        }

        private Calificacion calificacion;

        public Calificacion Calificacion
        {
            get { return calificacion; }
            set { calificacion = value; OnPropertyChanged(); }
        }


        private Oficio oficio;

        public Oficio Oficio
        {
            get { return oficio; }
            set { oficio = value; OnPropertyChanged(); }
        }

        private CustomSolicitud solicitudSeleccionada;

        public CustomSolicitud SolicitudSeleccionada
        {
            get { return solicitudSeleccionada; }
            set { solicitudSeleccionada = value; OnPropertyChanged(); }
        }


        public async void ObtenerOficio()
        {
            Oficio = new Oficio();
            try
            {
                Oficio = await oficioService.LeerOficio(SolicitudSeleccionada.IdOficio);
            }
            catch (Exception)
            {

            }

        }

        public async void ObtenerCalificacionSolicitud()
        {
            ExisteCalificacionSolicitud = await calificacionService.LeerCalificacionSolicitud(SolicitudSeleccionada.Id);
        }

        public Command CompletarSolicitudCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    bool eleccion = false;
                    if (Usuario.Tipo == 1) //Si es socio
                    {
                        eleccion = await App.Current.MainPage.DisplayAlert("Solicitud", "¿Está seguro de finalizar el requerimiento?", "Si", "No");
                    }
                    else if(Usuario.Tipo == 2) //Si es cliente
                    {
                        eleccion = await App.Current.MainPage.DisplayAlert("Solicitud", "¿Está seguro de finalizar la solicitud?", "Si", "No");
                    }

                    if (eleccion)
                    {
                        bool resultado = await solicitudService.FinalizarSolicitud(SolicitudSeleccionada.Id);
                        EstadoSolicitud = 2;
                    }
                    else
                    {

                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command CalificarSolicitudCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;
                    if (EstadoSolicitud == 2)
                    {
                        await App.Current.MainPage.Navigation.PushAsync(new Views.Calificacion());
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Primero debe completar la solicitud y luego podrá calificar al socio.", "OK");
                    }
                    
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command VolverAlMenuCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;

                    await App.Current.MainPage.Navigation.PushAsync(new Views.Menu());
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command VerCalificacionCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;

                    await App.Current.MainPage.Navigation.PushAsync(new Views.Calificacion());
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command EnviarCalificacionCommand
        {
            get
            {
                return new Command(async () =>
                {


                    IsBusy = true;
                    

                    if (SelectedIndexTipo == 0)
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una calificación", "OK");
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Comentario))
                        {
                            await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un comentario", "OK");
                        }
                        else
                        {
                            Calificacion.Comentario = Comentario;

                            Calificacion.IdUsuario = UsuarioContraparte.Id;
                            Calificacion.Puntuacion = SelectedIndexTipo;
                            bool resultado = await calificacionService.GuardarCalificacion(Calificacion);
                            if (resultado)
                            {
                                await App.Current.MainPage.DisplayAlert("Calificación", "Su calificación fue enviada", "OK");

                                await App.Current.MainPage.Navigation.PushAsync(new Views.Menu());
                            }
                            else
                            {
                                await App.Current.MainPage.DisplayAlert("Error", "Hubo un error en el envio de su calificación. Intente más tarde.", "OK");
                            }
                            
                        }


                    }

                    
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }



        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
