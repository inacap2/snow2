﻿using Snow.Models;
using Snow.Services;
using Snow.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.OpenWhatsApp;

namespace Snow.ViewModels
{
    public class InfoUsuarioViewModel : INotifyPropertyChanged
    {
        UsuarioService usuarioService = new UsuarioService();
        OficioService oficioService = new OficioService();
        SolicitudService solicitudService = new SolicitudService();
        MenuService menuService = new MenuService();
        CalificacionService calificacionService = new CalificacionService();
        

        public InfoUsuarioViewModel(){

            
            obtenerAsociacion();
            obtenerSocio();
            
            obtenerOficio();
            Cliente = menuService.ObtenerUsuario();
            obtenerCalificacion();

        }

        private Usuario socio;

        public Usuario Socio
        {
            get { return socio; }
            set { socio = value; OnPropertyChanged(); }
        }

        private Usuario cliente;

        public Usuario Cliente
        {
            get { return cliente; }
            set { cliente = value; OnPropertyChanged(); }
        }

        private string calificacionExacta;

        public string CalificacionExacta
        {
            get { return calificacionExacta; }
            set { calificacionExacta = value; OnPropertyChanged(); }
        }


        private AsociacionOficio asociacionOficio;

        public AsociacionOficio AsociacionOficio
        {
            get { return asociacionOficio; }
            set { asociacionOficio = value; OnPropertyChanged(); }
        }

        private Oficio oficio;

        public Oficio Oficio
        {
            get { return oficio; }
            set { oficio = value; OnPropertyChanged(); }
        }

        private Solicitud solicitud;

        public Solicitud Solicitud
        {
            get { return solicitud; }
            set { solicitud = value; OnPropertyChanged(); }
        }


        private string estadoSocio;

        public string EstadoSocio
        {
            get { return estadoSocio; }
            set { estadoSocio = value; OnPropertyChanged(); }
        }


        private string nombreCompleto;

        public string NombreCompleto
        {
            get { return Socio.Nombre + " " + Socio.Apellido; }
            set
            {
                nombreCompleto = value;
                OnPropertyChanged();
            }
        }


        public void obtenerAsociacion()
        {
            var _platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteConnection db = _platform.GetConnection();

            var respuesta = db.Table<AsociacionOficio>().First();
            if (respuesta.IdSocio != 0)
            {
                AsociacionOficio = respuesta;
            }

        }

        public async void obtenerSocio()
        {
            var respuesta = await usuarioService.Leer(AsociacionOficio.IdSocio);
            if (respuesta != null)
            {
                Socio = respuesta;
            }
        }

        public async void obtenerOficio()
        {
            var respuesta = await oficioService.LeerOficio(AsociacionOficio.IdOficio);
            if (respuesta != null && respuesta.Id != 0)
            {
                Oficio = respuesta;
            }
        }

        public async void obtenerCalificacion()
        {
            CalificacionExacta = await calificacionService.LeerCalificacionExacta(Socio.Id);
           
        }

        public Command SolicitarCommand
        {
            get
            {
                return new Command( async () =>
                {
                    IsBusy = true;
                    Solicitud = new Solicitud();
                    //Creo emparejamiento
                    Emparejamiento emparejamiento = new Emparejamiento()
                    {
                        Nombre = Socio.Nombre + " " + Oficio.Nombre
                    };

                    int idEmparejamiento = 0;

                    bool resultadoIngresoEmp = await solicitudService.GuardarEmparejamiento(emparejamiento);

                    if (!resultadoIngresoEmp)
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "No se pudo crear el emparejamiento", "OK");
                    }
                    else
                    {
                        idEmparejamiento = await solicitudService.LeerUltimoEmparejamiento();
                    }

                    //Completo los datos del objeto Solicitud

                    Solicitud.IdEmparejamiento = idEmparejamiento;
                    Solicitud.IdCliente = Cliente.Id;
                    Solicitud.IdSocio = Socio.Id;
                    Solicitud.IdOficio = Oficio.Id;
                    Solicitud.TipoSolicitud = 1; //Esto es fijo, ya que ahora todas las solicitudes son de parte del cliente



                    var respuesta = await App.Current.MainPage.DisplayAlert("¿Seguro?", "¿Está seguro de pedir una solicitud?", "Si", "No");

                    if (respuesta)
                    {
                        await App.Current.MainPage.Navigation.PushAsync(new Views.ResultadoSolicitud());
                        bool respuesta2 = await solicitudService.GuardarSolicitud(Solicitud);

                        if (respuesta2)
                        {

                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("Error", "No se pudo crear la solicitud", "OK");
                        }
                    }
                    else
                    {

                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command VolverMenuCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;


                    await App.Current.MainPage.Navigation.PushAsync(new Views.Menu());

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command EnviarMensajeCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;


                    try
                    {
                        var message = new SmsMessage("Mensaje solicitud", new[] { Socio.Telefono.ToString() });
                        await Sms.ComposeAsync(message);
                    }
                    catch (FeatureNotSupportedException)
                    {
                        // Sms is not supported on this device.
                    }
                    catch (Exception)
                    {
                        // Other error has occurred.
                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command EnviarCorreoCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;


                    try
                    {
                        var message = new EmailMessage
                        {
                            Subject = "Contacto "+Oficio.Nombre,
                            Body = "Mensaje predeterminado",
                            To = new List<string> { Socio.Mail.ToString() },
                            //Cc = ccRecipients,
                            //Bcc = bccRecipients
                        };
                        await Email.ComposeAsync(message);
                    }
                    catch (FeatureNotSupportedException)
                    {
                        // Email is not supported on this device
                    }
                    catch (Exception)
                    {
                        // Some other exception occurred
                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command HacerLlamadaCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;


                    try
                    {
                        PhoneDialer.Open(Socio.Telefono.ToString());
                    }
                    catch (ArgumentNullException)
                    {
                        // Number was null or white space
                    }
                    catch (FeatureNotSupportedException)
                    {
                        // Phone Dialer is not supported on this device.
                    }
                    catch (Exception)
                    {
                        // Other error has occurred.
                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }

        public Command EnviarWhatsappCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsBusy = true;


                    try
                    {
                        string numero = "+56" + Socio.Telefono.ToString();
                        Chat.Open(numero, "Hola " + Socio.Nombre);
                    }
                    catch (Exception)
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Hubo un error al enviar mensaje", "OK");
                    }

                    IsBusy = false;
                }, () => !IsBusy);

            }
        }


        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
