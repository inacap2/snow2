﻿using Snow.Models;
using Snow.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Snow.ViewModels
{
    public class EstadoSolicitudViewModel : INotifyPropertyChanged
    {
        UsuarioService usuarioService = new UsuarioService();
        OficioService oficioService = new OficioService();
        //Usuario usuario = new Usuario();
        MenuService menuService = new MenuService();
        SolicitudService solicitudService = new SolicitudService();




        public EstadoSolicitudViewModel()
        {
            try
            {
                SelectedSolicitud = new CustomSolicitud();
                Usuario = menuService.ObtenerUsuario();

                ObtenerSolicitudes();

                EstablecerNombre();

                ObtenerSolicitudSeleccionadaGuardada();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

        }

        private Usuario usuario;

        public Usuario Usuario
        {
            get { return usuario; }
            set { usuario = value; OnPropertyChanged(); }
        }

        private Usuario usuarioContraparte;

        public Usuario UsuarioContraparte
        {
            get { return usuarioContraparte; }
            set { usuarioContraparte = value; OnPropertyChanged(); }
        }

        private string nombreCompletoContraparte;

        public string NombreCompletoContraparte
        {
            get { return nombreCompletoContraparte; }
            set { nombreCompletoContraparte = value; OnPropertyChanged(); }
        }



        private List<CustomSolicitud> listCustomSolicitudes;

        public List<CustomSolicitud> ListCustomSolicitudes
        {
            get { return listCustomSolicitudes; }
            set { listCustomSolicitudes = value; OnPropertyChanged(); }
        }

        private List<Oficio> listOficios;

        public List<Oficio> ListOficios
        {
            get { return listOficios; }
            set { listOficios = value; OnPropertyChanged(); }
        }

        private Oficio oficio;

        public Oficio Oficio
        {
            get { return oficio; }
            set { oficio = value; OnPropertyChanged(); }
        }



        private CustomSolicitud selectedSolicitud;

        public CustomSolicitud SelectedSolicitud
        {
            get { return selectedSolicitud; }
            set { selectedSolicitud = value; OnPropertyChanged(); }
        }

        public async void ObtenerSolicitudes()
        {
            ListCustomSolicitudes = await solicitudService.LeerSolicitudes(Usuario.Id);
        }

        public async void ObtenerContraparte()
        {
            try
            {
                if (Usuario.Tipo == 1) //si es socio
                {
                    UsuarioContraparte = await usuarioService.Leer(SelectedSolicitud.IdCliente);
                }
                else if (Usuario.Tipo == 2) //Si es cliente
                {
                    UsuarioContraparte = await usuarioService.Leer(SelectedSolicitud.IdSocio);
                }
                //NombreCompletoContraparte = null;
                NombreCompletoContraparte = UsuarioContraparte.Nombre + " " + UsuarioContraparte.Apellido;
                ObtenerOficio();
            }
            catch (Exception)
            {

            }

        }

        public async void ObtenerSolicitudSeleccionadaGuardada()
        {
            var platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteAsyncConnection db = platform.GetAsyncConnection();
            try
            {
                SelectedSolicitud = await db.Table<CustomSolicitud>().FirstOrDefaultAsync();

                ObtenerContraparte();
            }
            catch (Exception)
            {

            }

        }

        public async void ObtenerOficio()
        {
            Oficio = new Oficio();
            try
            {
                Oficio = await oficioService.LeerOficio(SelectedSolicitud.IdOficio);
            }
            catch (Exception)
            {

            }

        }


        public async void EstablecerNombre()
        {
            IsBusy = true;
            ListOficios = await oficioService.LeerOficios();
            int contador = 0;
            for (int i = 0; i < ListCustomSolicitudes.Count; i++)
            {
                for (int j = 0; j < ListOficios.Count; j++)
                {
                    if (ListOficios[j].Id == ListCustomSolicitudes[i].IdOficio)
                    {
                        string vigencia = "";
                        if (ListCustomSolicitudes[i].Estado == 1)
                        {
                            vigencia = "Vig.";
                        }
                        else
                        {
                            vigencia = "Fin.";
                        }
                        contador++;
                        ListCustomSolicitudes[i].NombreVisible = "Solicitud N° " + contador + " Oficio: " + ListOficios[j].Nombre + " Estado: " + vigencia;
                    }
                }
            }
            IsBusy = false;
        }


        public Command BuscarSolicitudSelCommand
        {
            get
            {
                return new Command(async () =>
                {

                    IsBusy = true;
                    //Guardo en una base de datos SQLite la informacion del oficio seleccionado, para no tener que volver a consultar
                    var platform = DependencyService.Get<ISQLiteConexion>();
                    SQLiteAsyncConnection db = platform.GetAsyncConnection();

                    await db.DropTableAsync<CustomSolicitud>();
                    await db.CreateTableAsync<CustomSolicitud>();


                    //Aqui debe insertar el oficio en la BD local
                    await db.InsertAsync(SelectedSolicitud);



                    await App.Current.MainPage.Navigation.PushAsync(new Views.DetalleSolicitud());
                    IsBusy = false;
                }, () => !IsBusy);

            }
        }



        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}