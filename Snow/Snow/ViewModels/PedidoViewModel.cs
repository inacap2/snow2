﻿using Plugin.Geolocator;
using Snow.Models;
using Snow.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Snow.ViewModels
{
    public class PedidoViewModel : INotifyPropertyChanged
    {
        private Position _position;
        PosicionSocioService posicionSocioService = new PosicionSocioService();
        UsuarioService usuarioService = new UsuarioService();
        OficioService oficioService = new OficioService();
        CalificacionService calificacionService = new CalificacionService();

        

        public PedidoViewModel()
        {

            Map1 = new Map();
            EstablecerMapa();

            var _platform = DependencyService.Get<ISQLiteConexion>();
            SQLiteConnection db = _platform.GetConnection();
        }

        private List<Posicion> posiciones;

        public List<Posicion> Posiciones
        {
            get { return posiciones; }
            set { posiciones = value; OnPropertyChanged(); }
        }


        private Map map1;

        public Map Map1
        {
            get { return map1; }
            set { map1 = value;
                OnPropertyChanged();}
        }



        public void EstablecerMapa()
        {
            GetPosition(); //Primer intento de obtener la posición


            Map1.HeightRequest = 450;
            Map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-33.45, -70.69), Distance.FromMiles(3)));
            Map1.IsShowingUser = true;
            Map1.VerticalOptions = LayoutOptions.FillAndExpand;
            

        }


        public async void GetPosition()
        {
            
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            var posicion = await locator.GetPositionAsync(TimeSpan.FromSeconds(2), null, true);
            _position = new Position(posicion.Latitude, posicion.Longitude);
            
        }


        public Command BuscarEnZonaCommand
        {
            get
            {
                return new Command(async () =>
                {
                    await LeerSociosActuales();
                }, () => !IsBusy);

            }
        }


        public async Task LeerSociosActuales()
        {
            try
            {
                IsBusy = true;
                GetPosition();

                //Obtengo el oficio seleccionado por el cliente (esta guardado en la bd local)
                var platform = DependencyService.Get<ISQLiteConexion>();
                SQLiteConnection db = platform.GetConnection();
                var oficioSeleccionado = db.Table<Oficio>().First();


                int tipoError = 0;
                int contadorSocioActivos = 0;

                Posiciones = await posicionSocioService.LeerPosiciones();
                Map1.Pins.Clear();
                if (Posiciones.Count > 0)
                {
                    for (int i = 0; i < Posiciones.Count; i++)
                    {
                        
                        var socioActual = await usuarioService.Leer(Posiciones[i].IdUsuario);
                        var posicionActual = new Position(Posiciones[i].Latitud, Posiciones[i].Longitud);
                        var asociacionActual = await oficioService.LeerAsociacion(socioActual.Id);
                        var calificacionActual = await calificacionService.LeerCalificacionExacta(socioActual.Id);





                        if (oficioSeleccionado.Id == asociacionActual.IdOficio
                            && asociacionActual.IdSocio == socioActual.Id && asociacionActual.estado == 1)
                        {
                            contadorSocioActivos++;
                            tipoError = 0;
                            CustomPin customPin = new CustomPin();

                            //Pin pinOficio = new Pin();
                            customPin.Label = socioActual.Nombre + " " + socioActual.Apellido;
                            customPin.Position = posicionActual;
                            customPin.Address = "Calificación promedio: " +calificacionActual;
                            customPin.Type = PinType.Place;
                            customPin.idSocio = socioActual.Id;
                            Map1.Pins.Add(customPin);
                            customPin.Clicked += async (sender, e) =>
                            {
                                var respuesta = await App.Current.MainPage.DisplayAlert("Pedido", "Socio " + customPin.Label, "Ver detalle del socio", "Cancelar");
                                if (respuesta)
                                {

                                //Inserto una asociacionUsuario en la bd local
                                db.DropTable<AsociacionOficio>();
                                    db.CreateTable<AsociacionOficio>();

                                    db.Insert(asociacionActual);

                                //Ir a la pagina de la informacion del usuario, en este caso del socio
                                await App.Current.MainPage.Navigation.PushAsync(new Views.InfoUsuario());

                                }
                                else
                                {
                                //Hacer nada
                            }
                            };


                        }
                        else if (oficioSeleccionado.Id != asociacionActual.IdOficio && asociacionActual.IdSocio == socioActual.Id && contadorSocioActivos==0)
                        {
                            tipoError = 1;
                        }


                    }
                    if (tipoError == 1)
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "No hay socios en este momento que ofrezcan el servicio", "OK");
                    }


                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "No hay socios en este momento, favor intente más tarde", "OK");
                }
            }
            catch(Exception)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Ha ocurrido un error, favor intente más tarde", "OK");
            }
            finally
            {
                IsBusy = false;
            }
        }


        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
