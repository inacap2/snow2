﻿using Snow.Models;
using Snow.Services;
using Snow.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SQLite;

namespace Snow.ViewModels
{
    public class UsuarioViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<Usuario> Usuarios { get; set; }
        UsuarioService usuarioService = new UsuarioService();

        private Usuario usuario = new Usuario();
        
        public Usuario Usuario
        {
            get { return usuario; }
            set
            {
                usuario = value;
                OnPropertyChanged();
               
            }
        }

        private List<Tipo> tipos;

        public List<Tipo> Tipos
        {
            get { return tipos; }
            set { tipos = value; OnPropertyChanged(); }
        }

        private int selectedIndexTipo;

        public int SelectedIndexTipo
        {
            get { return selectedIndexTipo; }
            set { selectedIndexTipo = value; OnPropertyChanged(); }
        }


        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; OnPropertyChanged(); }
        }

        private bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; OnPropertyChanged(); }
        }



        public Command GuardarCommand { get; set; }
        public Command LeerCommand { get; set; }

        public Command LoginCommand
        { get
            {
                
                return new Command( async () =>
                {
                    IsBusy = true;
                    var lecturaUsuario = usuarioService.Leer(usuario.Mail, usuario.Password).Result;
                        if (lecturaUsuario.Id != 0)
                        {

                            //Guardo en una base de datos SQLite la informacion del usuario actual, para no tener que volver a consultar
                            var platform = DependencyService.Get<ISQLiteConexion>();
                            SQLiteAsyncConnection db = platform.GetAsyncConnection();

                            await db.DropTableAsync<Usuario>();
                            await db.CreateTableAsync<Usuario>();
                        

                            //Aqui debe insertar el usuario en la BD local
                            await db.InsertAsync(lecturaUsuario);
                        
                         

                            await App.Current.MainPage.DisplayAlert("Logueado", "Usted ha iniciado sesión con éxito", "OK");
                            await App.Current.MainPage.Navigation.PushAsync(new Views.Menu());

                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("Error", "No se ha podido iniciar sesión, favor intente nuevamente", "OK");
                        }

                    IsBusy = false;

                }, () => !IsBusy);
                    
            }
        }


        public UsuarioViewModel()
        {

            Tipos = new List<Tipo>();
            Tipo tipo1 = new Tipo
            {
                Id = 1,
                Nombre = "Socio"
            };
            Tipo tipo2 = new Tipo
            {
                Id = 2,
                Nombre = "Cliente"
            };

            //Tipos.Add(new Tipo());
            Tipos.Add(tipo1);
            Tipos.Add(tipo2);

            Usuarios = usuarioService.Consultar();

            GuardarCommand = new Command(async () =>
            {
                IsBusy = true;
                if (SelectedIndexTipo == 0)
                {
                    usuario.Tipo = 1;
                }
                if (SelectedIndexTipo == 1)
                {
                    usuario.Tipo = 2;
                }
                var exito = await usuarioService.Guardar(usuario);
                if (exito)
                {
                    await App.Current.MainPage.DisplayAlert("Felicidades", "Usted se ha registrado con éxito", "OK");
                    await App.Current.MainPage.Navigation.PushAsync(new Views.Login());
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "No se ha podido registrar, intente más tarde", "OK");
                };
                IsBusy = false;
            }, () => !IsBusy);

            LeerCommand = new Command(async () => { IsBusy = true; await usuarioService.Leer(usuario.Id); IsBusy = false; }, () => !IsBusy);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



    }
}
