﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Snow.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registro : ContentPage
	{
		public Registro ()
		{
            

            InitializeComponent ();
            ValidarCampos();
        }

        public void ValidarCampos()
        {

            if (String.IsNullOrEmpty(Nombre_Entry.Text) ||
                String.IsNullOrEmpty(Apellido_Entry.Text) ||
                String.IsNullOrEmpty(Rut_Entry.Text) ||
                String.IsNullOrEmpty(Telefono_Entry.Text) || 
                String.IsNullOrEmpty(Mail_Entry.Text) ||
                String.IsNullOrEmpty(Password_Entry.Text))
            {
                GuardarRegistro_Button.IsEnabled = false;
            }
            Telefono_Entry.Text = "";

            int Errores = 0;
            Nombre_Entry.Unfocused += (o, e) =>
            {
                Lbl_Nombre.Text = "Nombre";
                Errores = 0;
                if (String.IsNullOrEmpty(Nombre_Entry.Text))
                {
                    Errores++;
                    Lbl_Nombre.TextColor = Color.Red;
                    Lbl_Nombre.Text = "Nombre -  Debe rellenar el campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Nombre.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }
                }
            };

            Apellido_Entry.Unfocused += (o, e) =>
            {
                Lbl_Apellido.Text = "Apellido";
                Errores = 0;
                if (String.IsNullOrEmpty(Apellido_Entry.Text))
                {
                    Errores++;
                    Lbl_Apellido.TextColor = Color.Red;
                    Lbl_Apellido.Text = "Apellido - Debe rellenar el campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Apellido.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }

                }
            };

            Rut_Entry.Unfocused += (o, e) =>
            {
                Errores = 0;
                Lbl_Rut.Text = "Rut";
                if (!String.IsNullOrEmpty(Rut_Entry.Text))
                {
                    Rut_Entry.Text = Rut_Entry.Text.Trim();
                }
                
                if (String.IsNullOrEmpty(Rut_Entry.Text))
                {
                    Errores++;
                    Lbl_Rut.TextColor = Color.Red;
                    Lbl_Rut.Text = "Rut - Debe rellenar el campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else if (esRutErroneo(Rut_Entry.Text))
                {
                    Lbl_Rut.TextColor = Color.Red;
                    Lbl_Rut.Text = "Rut - No es Rut válido.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Rut.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }

                }
            };

            Telefono_Entry.Unfocused += (o, e) =>
            {
                Lbl_Telefono.Text = "Teléfono";
                if (!String.IsNullOrEmpty(Telefono_Entry.Text))
                {
                    Telefono_Entry.Text = Telefono_Entry.Text.Trim();
                }
                    
                Errores = 0;
                if (String.IsNullOrEmpty(Telefono_Entry.Text))
                {
                    Errores++;
                    Lbl_Telefono.TextColor = Color.Red;
                    Lbl_Telefono.Text = "Teléfono -  Debe rellenar el campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Telefono.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }

                }
            };

            Mail_Entry.Unfocused += (o, e) =>
            {
                Lbl_Mail.Text = "Correo";

                if (!String.IsNullOrEmpty(Mail_Entry.Text))
                {
                    Mail_Entry.Text = Mail_Entry.Text.Trim();
                }
  
                Errores = 0;
                if (String.IsNullOrEmpty(Mail_Entry.Text))
                {
                    Errores++;
                    Lbl_Mail.TextColor = Color.Red;
                    Lbl_Mail.Text = "Correo - Debe rellenar el campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else if(esMailErroneo(Mail_Entry.Text))
                {
                    Lbl_Mail.TextColor = Color.Red;
                    Lbl_Mail.Text = "Correo - Formato de correo inválido.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Mail.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }

                }
            };

            Password_Entry.Unfocused += (o, e) =>
            {
                Lbl_Password.Text = "Password";

                if (String.IsNullOrEmpty(Password_Entry.Text))
                {
                    Password_Entry.Text = Password_Entry.Text.Trim();
                }

                Errores = 0;
                if (String.IsNullOrEmpty(Password_Entry.Text))
                {
                    Errores++;
                    Lbl_Password.TextColor = Color.Red;
                    Lbl_Password.Text = "Password - Debe rellenar este campo.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else if (esPassErronea(Password_Entry.Text))
                {
                    Lbl_Password.TextColor = Color.Red;
                    Lbl_Password.Text = "Password - Debe ser de al menos 8 caracteres.";
                    GuardarRegistro_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Password.TextColor = Color.Black;
                    if (Lbl_Nombre.TextColor == Color.Black &&
                        Lbl_Apellido.TextColor == Color.Black &&
                        Lbl_Rut.TextColor == Color.Black &&
                        Lbl_Telefono.TextColor == Color.Black &&
                        Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        GuardarRegistro_Button.IsEnabled = true;
                    }

                }
            };


        }

        public bool esMailErroneo(string entrada)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(entrada, expresion))
            {
                if (Regex.Replace(entrada, expresion, String.Empty).Length == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public bool esRutErroneo(string rut)
        {

            bool validacion = true;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = false;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }

        public bool esPassErronea(string pass)
        {
            if(pass.Length < 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    
    }
}