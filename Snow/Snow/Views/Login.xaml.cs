﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Snow.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            ValidarCampos();
        }

        public void ValidarCampos()
        {
            if (String.IsNullOrEmpty(Mail_Entry.Text) ||
                String.IsNullOrEmpty(Password_Entry.Text))
            {
                Entrar_Button.IsEnabled = false;
            }

            Mail_Entry.Unfocused += (o, e) =>
            {
                Lbl_Mail.Text = "Correo";

                if (!String.IsNullOrEmpty(Mail_Entry.Text))
                {
                    Mail_Entry.Text = Mail_Entry.Text.Trim();
                }

                if (String.IsNullOrEmpty(Mail_Entry.Text))
                {
                    Lbl_Mail.TextColor = Color.Red;
                    Lbl_Mail.Text = "Correo -  Debe rellenar el campo.";
                    Entrar_Button.IsEnabled = false;
                }
                else if (esMailErroneo(Mail_Entry.Text))
                {
                    Lbl_Mail.TextColor = Color.Red;
                    Lbl_Mail.Text = "Correo - Formato de correo inválido.";
                    Entrar_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Mail.TextColor = Color.Black;
                    if (Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        Entrar_Button.IsEnabled = true;
                    }

                }
            };

            Password_Entry.Unfocused += (o, e) =>
            {
                Lbl_Password.Text = "Contraseña";
                if (String.IsNullOrEmpty(Password_Entry.Text))
                {
                    Lbl_Password.TextColor = Color.Red;
                    Lbl_Password.Text = "Contraseña -  Debe rellenar el campo.";
                    Entrar_Button.IsEnabled = false;
                }
                else
                {
                    Lbl_Password.TextColor = Color.Black;
                    if (Lbl_Mail.TextColor == Color.Black &&
                        Lbl_Password.TextColor == Color.Black)
                    {
                        Entrar_Button.IsEnabled = true;
                    }

                }
            };

        }

        public bool esMailErroneo(string entrada)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(entrada, expresion))
            {
                if (Regex.Replace(entrada, expresion, String.Empty).Length == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private void RegistrarPrimeraVez(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new Registro());
        }
    }
 }