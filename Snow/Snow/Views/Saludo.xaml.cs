﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Snow.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Saludo : ContentPage
	{
		public Saludo ()
		{
			InitializeComponent ();
           
		}

        private void IniciarSesion(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new Login());
        }

        private void RegistrarPrimeraVez(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new Registro());
        }
    }
}